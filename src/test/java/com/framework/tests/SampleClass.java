package com.framework.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SampleClass {

	
	
	
	
	@Test
	public void testSample() {
		WebDriverManager.chromedriver().setup();
		
		
		WebDriver driver = new ChromeDriver();
		
		driver.get("http://www.google.com");
		
		Reporter.log("I am launching a website", true);
		
		
		Reporter.log("Get the title" + driver.getTitle());
		
		driver.quit();
	}
}



